#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "error.h"
#include "vector.h"

/* Imprime o vetor com todos os seus elementos. */
void printv(Vector* v)
{
    int pos;
    
    printf("v(%d/%d) -> [", v->used, v->size);
    
    for (pos = 0; pos < v->used; pos++)
        printf("%d%s",
            v->data[pos],
            pos < v->used-1 ? ", " : "");
            
    printf("]\n");
}

/* Imprime o elemento da posição 'pos' do vetor. */
void printe(Vector* v, unsigned int pos)
{
    if (pos < v->used)
        printf("v[%d] -> %d\n", pos, v->data[pos]);
}

void helper(void) {
    printf("uso: PROG [argumentos]\n"
           "\nonde os argumentos são:"
           "\n\t i n   : Insere o número 'n' na posição 'i' do vetor."
           "\n\t-i     : Remove o elemento de índice 'i' do vetor."
           "\n\t f n   : Busca o primeiro 'n' no vetor e imprime 'v[i] -> n'."
           "\n\t c n   : Imprime a quantidade de ocorrências de 'n' no vetor."
           "\n\t e i   : Imprime 'v[i] -> n'."
           "\n\t print : Imprime o vetor.\n");
}

int main(int argc, char** argv)
{
    int index;
    Vector *v = VectorCreate(2);
    
    if(argc < 2)
        helper();
    
    if (v == NULL)
        return MEMORY_E;
    
    for (index = 1; index < argc; index++) {
        switch (argv[index][0]) {
            case 'e':
                printe(v, atoi(argv[++index]));
            break;
            
            case 'f':
                printe(v, VectorFind(v, atoi(argv[++index])));
            break;
        
            case 'c':
                printf("%d\n", VectorCount(v, atoi(argv[++index])));
            break;
            
            case 'p':
                if (!strcmp(argv[index], "print"))
                    printv(v);
            break;
        
            case '-':
                VectorRemove(v, -atoi(argv[index]));
            break;
            
            default:
                VectorInsert(v, atoi(argv[index]), atoi(argv[index+1]));
                index++;
            break;    
        }
    }
    
    VectorDestroy(v);
    
    return SUCCESS;
}
