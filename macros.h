#ifndef MACROS_H
#define MACROS_H

/* retorna o MENOR valor entre 'a' e  'b' */
#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* retorna o MAIOR valor entre 'a' e  'b' */
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#endif