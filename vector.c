﻿#include "error.h"
#include "macros.h"
#include "vector.h"
#include <stdlib.h>

Vector* VectorCreate(int block) {

	Vector *v = NULL;
	if (block > 0) {

		v = (Vector*) malloc(sizeof(Vector));

		if (v != NULL) {
			v->data = (int*) calloc(block, sizeof(int));
			if (v->data == NULL) {
				VectorDestroy(v);
				return NULL;
			}
			v->block = block;
			v->used = 0;
			v->size = block;
		}

		return v;
	}

	return NULL;
}

void VectorDestroy(Vector* v) {

	free(v->data);
	free(v);
	v = NULL;
}

static int VectorResize(Vector* v, int new_size) {

	int *aux = realloc(v->data, new_size * sizeof(int));
	if (aux != NULL) {
		v->data = aux;
		v->size = new_size;
		return SUCCESS;
	}

	return MEMORY_E;

}

int VectorInsert(Vector* v, int pos, int data) {

	if (pos < 0) {
		return PARAM_E;
	}

	if (v->size == v->used) {
		VectorResize(v, v->size + v->block);
	}

	int k;
	pos = MIN(pos, v->used);
	for (k = v->used; k > pos; k--) {
		v->data[k] = v->data[k - 1];
	}

	v->data[pos] = data;
	v->used++;

	return SUCCESS;
}

int VectorRemove(Vector* v, int pos) {

	if (pos < 0 || pos > v->used - 1) {
		return PARAM_E;
	}

	if (v->used == 0) {
		return PARAM_E;
	}

	int k;
	for (k = pos; k < v->used - 1; k++) {
		v->data[k] = v->data[k + 1];
	}
	v->used--;

	if (v->size - v->used >= 2 * v->block)
		VectorResize(v, v->size - v->block);

	return SUCCESS;
}

int VectorFind(Vector* v, int data) {

	int k;
	for (k = 0; k <= v->size; k++) {
		if (v->data[k] == data)
			return k;
	}

	return -1;
}

int VectorCount(Vector* v, int data) {

	int k;
	int cont = 0;
	for (k = 0; k <= v->size; k++) {
		if (v->data[k] == data)
			cont++;
	}

	return cont;
}
